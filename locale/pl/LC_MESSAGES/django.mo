��          �      �           	            
   '  
   2     =     M     Q     Y     ^  �  k                    ,     1     9  
   @     K  	   `  	   j     t  �  {     P     W     n     v     �     �     �     �     �     �  �  �     }     �     �     �     �     �  
   �     �     �     �     �                         	                     
                                                          About me Android app  Contact Contact Me Experience Git Repository  Hey Hire Me Home I am Matthew I am an ambitious high school student who is passionate about science, economy and financial markets. Through my education I took part in many competitions and olympiads. My greatest achievement was getting to the final of the team IT Olympics in middle school and receiving scholarships from Rada Powiatu Wrocławskiego and Akademickie Liceum Ogólnokształcącej Politechniki Wrocławskiej for my science achievements. Let me Message My Portfolio Name Subject Submit This page  Work in progress...  get my cv introduce myself Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 O mnie Aplikacja na androida  Kontakt Skontaktuj się ze mną Doświadczenie Repozytorium Git  Cześć Zatrudnij mnie Strona Główna Jestem Mateusz Jestem ambitnym uczniem liceum, który pasjonuje się programowaniem, naukami ścisłymi oraz ekonomią i rynkami kapitałowymi. Na swojej ścieżce edukacyjnej z sukcesami uczestniczyłem w wielu konkursach i olimpiadach. Zostałem między innymi finalistą drużynowej Olimpiady Informatycznej Gimnazjalistów, stypendystą Rady Powiatu Wrocławskiego oraz Akademickiego Liceum Politechniki Wrocławskiej. Pozwól, Treść wiadomości Moje Portfolio Imię Temat Wyślij Ta strona  Praca w toku...  moje cv że się przedstawię 