from django.shortcuts import render
from django.utils import translation
from django.utils.translation import gettext
from django.utils.translation import get_language_from_request
from django.core.mail import send_mail



def home(request):
    #user_language = 'pl'
    #translation.activate(user_language)
    #request.session[translation.LANGUAGE_SESSION_KEY] = user_language

    #if translation.LANGUAGE_SESSION_KEY in request.session:
        #del request.session[translation.LANGUAGE_SESSION_KEY]

    if get_language_from_request(request, check_path=False) == 'en':
        home_butttons_class = 'english-home-buttons'
    elif get_language_from_request(request, check_path=False) == 'pl':
        home_butttons_class = 'polish-home-buttons'

    buttons = {
        'home': 'nav-item active',
        'about': 'nav-item',
        'experience': 'nav-item',
        'portfolio': 'nav-item',
        'contact': 'nav-item',
        'home_buttons_class': home_butttons_class,
    }

    return render(request, 'blog/home.html', buttons)


def about(request):
    buttons = {
        'home': 'nav-item',
        'about': 'nav-item active',
        'experience': 'nav-item',
        'portfolio': 'nav-item',
        'contact': 'nav-item'
    }
    return render(request, 'blog/about_me.html', buttons)


def experience(request):
    if get_language_from_request(request, check_path=False) == 'en':
        experience = 'Experience'
    elif get_language_from_request(request, check_path=False) == 'pl':
        experience = 'Doświadczenia'
    context = {
        'home': 'nav-item',
        'about': 'nav-item',
        'experience': 'nav-item active',
        'portfolio': 'nav-item',
        'contact': 'nav-item',
        'odmienione': experience

    }
    return render(request, 'blog/experience.html', context)


def portfolio(request):
    context = {
        'home': 'nav-item',
        'about': 'nav-item',
        'experience': 'nav-item',
        'portfolio': 'nav-item active',
        'contact': 'nav-item'
    }

    return render(request, 'blog/portfolio.html', context)


def contact(request):
    context = {
        'home': 'nav-item',
        'about': 'nav-item',
        'experience': 'nav-item',
        'portfolio': 'nav-item',
        'contact': 'nav-item active',
        'toast_class': 'toast toast-hide',
        'toast_header': '',
        'toast_body': '',
    }

    if request.method == 'POST':
        message_name = request.POST['message-name']
        message_email = request.POST['message-email']
        message = request.POST['message']
        message_subject = request.POST['message-subject']

        if message_email.find('@') == -1 or message_email.find('.') == -1:
            context['toast_class'] = 'toast toast-show'
            context['toast_header'] = 'Błąd wysłania wiadomości'
            context['toast_body'] = "Wprowadzony email jest niepoprawny"
            return render(request, 'blog/contact.html', context)
        else:
            # send an email
            send_mail('From ' + message_name + ' about ' + message_subject, message + '\n' + message_email, message_email, ['mateuszkelner@gmail.com'], fail_silently=False)
            # order - subject, message, from email, to email
            context['toast_class'] = 'toast toast-show'
            context['toast_header'] = 'Pomyślnie wysłano wiadomość'
            context['toast_body'] = "Udało się wysłać wiadomość, niedługo otrzymasz odpowiedź"
            return render(request, 'blog/contact.html', context)
    else:
        return render(request, 'blog/contact.html', context)
