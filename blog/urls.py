from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='blog-home'),
    path('about/', views.about, name='blog-about'),
    path('experience/', views.experience, name='blog-experience'),
    path('portfolio/', views.portfolio, name='blog-portfolio'),
    path('contact/', views.contact, name='blog-contact'),
]
